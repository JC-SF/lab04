package inheritance;
public class ElectronicBook extends Book{
	private int numberBytes;
	//private String author;
	public ElectronicBook(String v_author, String v_title, int v_numberBytes) {
		super(v_author,v_title);
		//author = v_author;
		numberBytes = v_numberBytes;
	}
	public String toString() {
		return super.toString()+"\nNumber Of Bytes: "+numberBytes+" Bytes";
	}
}
