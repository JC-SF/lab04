package inheritance;

public class Book {
	protected String title;
	private String author;
	
	public Book(String v_author,String v_title) {
		title = v_title;
		author= v_author;
	}
	public String getTitle() {
		return title;
	}
	public String getAuthor() {
		return author;
	}
	public String toString() {
		return "Author: "+author+"\nTitle: "+title;
	}
}
