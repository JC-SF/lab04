package main;
import inheritance.*;
public class myBooks {
	public static void main(String[] args) {
		/*
		ElectronicBook myBook = new ElectronicBook("Juan", "How to be a Bear", 56);
		System.out.println(myBook);
		*/
		Book[] myBookList = new Book[5];
		myBookList[0] = new Book("Juan-Carlos Sreng-Flores", "Java For Pros");
		myBookList[1] = new ElectronicBook("John Molson", "Business For Dummies", 900);
		myBookList[2] = new Book("Brother Inc.", "Printer Guide");
		myBookList[3] = new ElectronicBook("Thomas H. Cormen", "Introduction to Algorithm 3rd", 5499000);
		myBookList[4] = new ElectronicBook("Eric Dubay", "The Flat-Earth Conspiracy", 18);
		
		for (Book book : myBookList) {
			System.out.println(book);
			System.out.println();
		}
	}
}
