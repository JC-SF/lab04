package geometry;
import geometry.Shape;
public class Rectangle implements Shape {
	private double length;
	private double width;
	public Rectangle(double v_length, double v_width) {
		length = v_length;
		width = v_width;
	}
	public double getArea() {
		return length*width;
	}
	public double getPerimeter() {
		return 2*(width+length);
	}
	public double getLength() {
		return length;
	}
	public double getWidth() {
		return width;
	}
}