package geometry;
import geometry.Shape;
import geometry.Rectangle;
public class Square extends Rectangle implements Shape{
	public Square(double sideLength) {
		super(sideLength,sideLength);
	}
	
}