package geometry;
import geometry.Shape;
public class LotsOfShapes {

	public static void main(String[] args) {
		Shape[] shapes = new Shape[5];
		shapes[0] = new Square(7);
		shapes[1] = new Circle(5);
		shapes[2] = new Rectangle(3,4);
		shapes[3] = new Rectangle(87,1);
		shapes[4] = new Circle(10);
		for(Shape shape : shapes) {
			System.out.println("Area: "+shape.getArea());
			System.out.println("Perimeter: "+shape.getPerimeter()+"\n");
		}
	}

}