package geometry;
import geometry.Shape;
public class Circle implements Shape{
	private double radius;
	public Circle(double v_radius) {
		radius = v_radius;
	}
	public double getRadius() {
		return radius;
	}
	public double getArea() {
		return Math.PI*radius*radius;
	}
	public double getPerimeter() {
		return Math.PI*2*radius;
	}
	
}